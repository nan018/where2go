﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Muzej.Entiteti;
using NHibernate.Linq;

namespace Muzej
{
    public class DTOManager
    {
        public static List<EksponatPregled> GetOdInfos()
        {
            List<EksponatPregled> odInfos = new List<EksponatPregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IQuery q = s.CreateQuery("from Eksponat");

                IList<Eksponat> eksponati = q.List<Eksponat>();

                foreach (Eksponat o in eksponati)
                {
                    odInfos.Add(new EksponatPregled(o.Id, o.IsPozajmljen, o.Tip, o.TipEksponata, (o.Umetnik!=null)? o.Umetnik.Id : 0));
                }

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }

            return odInfos;
        }

		public static EksponatPregled GetEksponatPregled(int ekId,int umId)
		{
			EksponatPregled ob = new EksponatPregled();
			try
			{
				ISession s = DataLayer.GetSession();

				Eksponat e = s.Load<Eksponat>(ekId);
				ob = new EksponatPregled(e.Id, e.IsPozajmljen, e.Tip, e.TipEksponata, umId);

				s.Close();

			}
			catch (Exception ec)
			{
				//handle exceptions
			}

			return ob;
		}

		public static EksponatPregled UpdateEksponatPregled(EksponatPregled ep)
		{
			try
			{
				ISession s = DataLayer.GetSession();

				Eksponat o = s.Load<Eksponat>(ep.EksponatId);
				o.IsPozajmljen = ep.Pozajmljen;
				o.Tip = ep.Tip;
				o.TipEksponata = ep.TipEksponata;

				s.Update(o);
				s.Flush();

				s.Close();

			}
			catch (Exception ec)
			{
				//handle exceptions
			}

			return ep;
		}
	}
}
