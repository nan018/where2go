﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Muzej.Entiteti;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace Muzej.Funkcije
{
    public class DonatorFunkcije
    {

        public static bool kreiraj_donatora(string adresa, string telefon, string ime, string oblastPoslovanja, bool isFirma)
        {

            try
            {
                Donator d = null;
                ISession s = DataLayer.GetSession();
                if(isFirma)
                     d =  new Firma();
                else
                     d = new Privatno();
                d.Adresa = adresa;
                d.Telefon = telefon;
                d.Ime = ime;

                if (d is Firma)
                    d.OblastPoslovanja = oblastPoslovanja;
                else
                    d.OblastPoslovanja = null; 
               

                s.Save(d);
                s.Close();
                return true;
            }
            catch (Exception er)
            {
                Console.WriteLine(er.Message);
                return false;
            }
        

        }

        public static bool izmeni_donatora(Int32 id, string adresa, string telefon, string ime, string isFirma)
        {

            try
            {

                ISession s = DataLayer.GetSession();

                Donator stari = null;
                if (isFirma == "Da")
                    stari = s.Load<Firma>(id);
                else
                    stari = s.Load<Privatno>(id);

              
                stari.Adresa = adresa;
                stari.Telefon = telefon;
                stari.Ime = ime;
            //    stari.IsFirma = isFirma;
            //    stari.OblastPoslovanja = oblastPoslovanja;

     
                s.Update(stari);
                s.Flush();
                s.Close();
                return true;
            }
            catch (Exception er)
            {
                Console.WriteLine(er.Message);
                return false;
            }




        }

        public static bool obrisi_donatora(Int32 id, bool isFirma)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                Donator stari = null;
                if(isFirma)
                   stari = s.Load<Firma>(id);
                else
                    stari = s.Load<Privatno>(id);

                s.Delete(stari);

                s.Flush();

                s.Close();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }


        }

        public static bool doniraj(Int32 donatorID, Int32 eksponatID, decimal iznos, DateTime datumDonacije)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                Donator d = s.Load<Donator>(donatorID);

                Eksponat eks = s.Load<Eksponat>(eksponatID);

                Donira donacija = new Donira();
                donacija.Iznos = iznos;
                donacija.Donator = d;
                donacija.Eksponat = eks;
                donacija.DatumDonacije = datumDonacije;
                s.Save(donacija);
                s.Flush();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

        }

        public static bool obrisi_donaciju(Int32 id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Donira donacija = s.Load<Donira>(id);

                s.Delete(donacija);

                s.Flush();

                s.Close();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

    }
}
