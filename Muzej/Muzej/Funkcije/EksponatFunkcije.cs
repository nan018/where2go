﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Muzej.Entiteti;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace Muzej.Funkcije
{
    public class EksponatFunkcije
    {   
        /// <summary>
        /// Funkcija kreira eksponat tipa TIP = "ostalo"
        /// </summary>
        /// <param name="tip"></param>
        /// <param name="tipEksponata"></param>
        /// <param name="isPozajmljen"></param>
        /// <param name="umetnikID"></param>
        /// <returns> True ako je eksponat uspesno kreiran u suprotnom false</returns>
        public static bool kreiraj_eksponat(string tipEksponata,string Tip, bool isPozajmljen, Int32 umetnikID)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Eksponat eksp = new Eksponat();
                eksp.TipEksponata = tipEksponata;
                eksp.Tip = Tip;
                if (isPozajmljen)
                    eksp.IsPozajmljen = "Da";
                else
                    eksp.IsPozajmljen = "Ne";

               Umetnik pikaso = s.Load<Muzej.Entiteti.Umetnik>(umetnikID);
               eksp.Umetnik = pikaso;
               s.Save(eksp);
			   s.Flush();
			   s.Close();
               return true;
            }
            catch (Exception ec)
            {
                Console.WriteLine(ec.Message);
                return false;
            }
        }

        public static bool kreiraj_pozajmljeno(string tip, string nazivKolekcije, string adresa, string opis, bool isprivateOwner, string tipEksponata, Int32 umetnikID)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                Pozajmljeno pozajmljen = new Pozajmljeno();

                pozajmljen.Opis = opis;
                pozajmljen.Tip = tip;
                pozajmljen.IsPozajmljen = "Da";
                pozajmljen.NazivKolekcije = nazivKolekcije;
                pozajmljen.IsPrivateOwner = (isprivateOwner)? "Da" : "Ne";
                pozajmljen.TipEksponata = tipEksponata;
                pozajmljen.Adresa = adresa;

                Umetnik pikaso = s.Load<Muzej.Entiteti.Umetnik>(umetnikID);
                pozajmljen.Umetnik = pikaso;


                s.Save(pozajmljen);
				s.Flush();
				s.Close();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }



        }

        public static bool izmeni_pozajmljeno(Int32 ID,string tip, string nazivKolekcije, string adresa, string opis, bool isprivateOwner, string tipEksponata, bool isPozajmljen, Int32 umetnikID)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Pozajmljeno pozajmljen = s.Load<Pozajmljeno>(ID);

                if (umetnikID != -1)
                {
                    Umetnik pikaso = s.Load<Muzej.Entiteti.Umetnik>(umetnikID);
                    pozajmljen.Umetnik = pikaso;
                }


                pozajmljen.Opis = opis;
                pozajmljen.Tip = tip;
                pozajmljen.IsPozajmljen = "Da";
                pozajmljen.NazivKolekcije = nazivKolekcije;
                pozajmljen.IsPrivateOwner = (isprivateOwner) ? "Da" : "Ne";
                pozajmljen.TipEksponata = tipEksponata;
                pozajmljen.Adresa = adresa;   
           


                s.Close();

              ISession s1 = DataLayer.GetSession();


              s1.Update(pozajmljen);
              s1.Flush();
              s1.Close();

                return true;
                
            }catch(Exception er)
            {
                Console.WriteLine(er.Message);
                return false;
            }

        }

        public static bool izmeni_eksponat(Int32 ID, string tip, string tipEksponata, Int32 umetnikID)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Eksponat eks = s.Load<Eksponat>(ID);

                if (umetnikID != -1) {
                    Umetnik pikaso = s.Load<Muzej.Entiteti.Umetnik>(umetnikID);
                    eks.Umetnik = pikaso;


                }

                eks.Tip = tip;
                eks.TipEksponata = tipEksponata;


                s.Close();

                ISession s1 = DataLayer.GetSession();


                s1.Update(eks);
                s1.Flush();
                s1.Close();

                return true;

            }
            catch (Exception er)
            {
                Console.WriteLine(er.Message);
                return false;
            }

        }

        public static bool izbrisi_pozajmljeno(Int32 ID)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Eksponat p = s.Load<Eksponat>(ID);

                s.Delete(p);
                s.Flush();
                s.Close();
           
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }

        }

        public static bool kreiraj_sliku(string tipEksponata, string Tip, bool isPozajmljen, Int32 umetnikID, string tehnika, string stil, bool isOriginalRam, string Materijal, decimal duzina, decimal sirina)
		{
			try
			{
				ISession s = DataLayer.GetSession();
				Slika slika = new Slika();
				//int id = s.Query<Eksponat>().Max(x => (int)x.Id);
				//slika.Id = id;
				slika.TipEksponata = tipEksponata;
				slika.Tip = Tip;
				if (isPozajmljen)
					slika.IsPozajmljen = "Da";
				else
					slika.IsPozajmljen = "Ne";
				Umetnik pikaso = s.Load<Muzej.Entiteti.Umetnik>(umetnikID);
				slika.Umetnik = pikaso;
				slika.Tehnika = tehnika;
				slika.Tip = Tip;
				slika.Stil = stil;
				if (isOriginalRam)
					slika.OriginalRam = "Da";
				else
					slika.OriginalRam = "Ne";
				slika.Materijal = Materijal;
				slika.Duzina = duzina;
				slika.Sirina = sirina;

				s.Save(slika);
				s.Flush();
				s.Close();

				return true;
			}
			catch (Exception ec)
			{
				Console.WriteLine(ec.Message);
				return false;
			}
        }

		public static bool kreiraj_statuu(string tipEksponata, string Tip, bool isPozajmljen, Int32 umetnikID, string stil, decimal tezina, string materijal, decimal visina)
		{ 
			try
			{
				ISession s = DataLayer.GetSession();
				Statua statua = new Statua();
				//int id = s.Query<Eksponat>().Max(x => (int)x.Id);
				//statua.ID = id;
				statua.TipEksponata = tipEksponata;
				statua.Tip = Tip;
				if (isPozajmljen)
					statua.IsPozajmljen = "Da";
				else
					statua.IsPozajmljen = "Ne";
				Umetnik pikaso = s.Load<Muzej.Entiteti.Umetnik>(umetnikID);
				statua.Umetnik = pikaso;
				statua.Stil = stil;
				statua.Tezina = tezina;
				statua.Materijal = materijal;
				statua.Visina = visina;

				s.Save(statua);
				s.Flush();
				s.Close();
				return true;
			}
			catch (Exception ec)
			{
				Console.WriteLine(ec.Message);
				return false;
			}

		}
	}
}
