﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Muzej.Entiteti;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace Muzej.Funkcije
{
    public class UmetnikFunkcije
    {
        public static bool kreiraj_umetnika(DateTime rodjenje, DateTime? smrt, string ime, string zemlja, string epoha, string stil, string tehnika)
        {
            try
            {
            ISession s = DataLayer.GetSession();

            Umetnik pikaso = new Umetnik();

            pikaso.DatumRodjenja = rodjenje;
            pikaso.DatumSmrti = smrt;
            pikaso.Ime = ime;
            pikaso.ZemljaPorekla = zemlja;
            pikaso.Epoha = epoha;
            pikaso.GlavniStil = stil;
            pikaso.GlavnaTehnika = tehnika;


            s.Save(pikaso);
            s.Close();
            return true;
            }
            catch (Exception ec)
            {
                Console.WriteLine(ec.Message);
                return false;
            }

        }

        public static bool izmeni_umetnika(Int32 ID, string epoha, string stil, string tehnika)
        {
            try
            {
               ISession s = DataLayer.GetSession();

                Umetnik um = s.Load<Umetnik>(ID);

                um.Epoha = epoha;
                um.GlavniStil = stil;
                um.GlavnaTehnika = tehnika;

                s.Close();

                ISession s1 = DataLayer.GetSession();

                s1.Update(um);
                s1.Flush();
                s1.Close();
                return true;
            }
            catch(Exception ec)
            {
                Console.WriteLine(ec.Message);
                return false;
            }
        }
        public static bool obrisi_umetnika(Int32 ID)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Umetnik um = s.Load<Umetnik>(ID);
                IList<Eksponat> listEksp = s.QueryOver<Eksponat>().Where(eks => eks.Umetnik == um).List<Eksponat>();
                foreach(Eksponat eks in listEksp)
                {
                    eks.Umetnik = null;
                    s.Update(eks);
                }
                s.Flush();
                s.Delete(um);
                s.Flush();
                s.Close();

                return true;
            }
            catch (Exception ec)
            {
                Console.WriteLine(ec.Message);
                return false;
            }
        }




    }
}
