﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate;
using NHibernate.Linq;
using Muzej.Entiteti;
using Muzej.DTOs;
using System.Web;
using System.Runtime.Serialization;
using Newtonsoft.Json.Linq;



namespace Muzej
{
    public class DataProvider
    {
        public IEnumerable<Eksponat> GetEksponati()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Eksponat> eksponati = s.Query<Eksponat>().Select(eks => eks);

            return eksponati;
        }
        public IEnumerable<Umetnik> GetUmetnici()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Umetnik> Umetnici = s.Query<Umetnik>().Select(eks => eks);

            return Umetnici;
        }

        public IEnumerable<Donator> GetDonatori()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Donator> donatori = s.Query<Donator>().Select(eks => eks);

            return donatori;
        }

        public IEnumerable<Donira> GetDonacije()
        {
            ISession s = DataLayer.GetSession();

            IEnumerable<Donira> donacije = s.Query<Donira>().Select(eks => eks);

            return donacije;
        }

        public Eksponat GetEksponat(int id)
        {

            ISession s = DataLayer.GetSession();
            Eksponat eks = s.Query<Eksponat>().Where(e => e.Id == id).FirstOrDefault(); ;
            return eks;
        }


        public Umetnik GetUmetnik(int id)
        {

            ISession s = DataLayer.GetSession();
            Umetnik pikaso = s.Query<Umetnik>().Where(e => e.Id == id).FirstOrDefault(); ;
            return pikaso;
        }
        public Donira GetDonaciju(int id)
        {

            ISession s = DataLayer.GetSession();
            Donira donacija = s.Query<Donira>().Where(e => e.Id == id).FirstOrDefault(); ;
            return donacija;
        }
        public Donator GetDonator(int id)
        {

            ISession s = DataLayer.GetSession();
            Donator don = s.Query<Donator>().Where(e => e.Id == id).FirstOrDefault(); ;
            return don;
        }
      
        public bool UpdateEksponat(int id, string tip, string tipE, int idUmet)
        {
            return Funkcije.EksponatFunkcije.izmeni_eksponat(id, tip, tipE, idUmet);
        }

        public bool AddEksponat(Object eks)
        {
             JObject  jEks = eks as JObject;
            
             Eksponat eksponat = new Eksponat();
             JProperty ss = jEks.Property("tip");

            eksponat.TipEksponata = jEks.Property("tipEksponata").Value.ToString();
            eksponat.Tip = ss.Value.ToString();
            //eksponat.IsPozajmljen = jEks.Property("isPozajmljen").Value.ToString();
            int idUmet = -1;
            if (jEks.Property("idUmetnika")!=null)
            {
                idUmet = Int32.Parse(jEks.Property("idUmetnika").Value.ToString());
            }

            switch (eksponat.Tip)
	        {
                case "slika":
                    return Funkcije.EksponatFunkcije.kreiraj_sliku(eksponat.TipEksponata, eksponat.Tip, false, idUmet, jEks.Property("tehnika").Value.ToString(), jEks.Property("stil").Value.ToString(),
                         (jEks.Property("isOriginalRam").Value.ToString() == "Da") ? true : false, jEks.Property("materijal").Value.ToString(),
                         Decimal.Parse(jEks.Property("duzina").Value.ToString()), Decimal.Parse(jEks.Property("sirina").Value.ToString()));
                case "statua":
                       return Funkcije.EksponatFunkcije.kreiraj_statuu(eksponat.TipEksponata, eksponat.Tip, false, idUmet, jEks.Property("stil").Value.ToString(),
                           Decimal.Parse(jEks.Property("tezina").Value.ToString()), jEks.Property("materijal").Value.ToString(), Decimal.Parse(jEks.Property("visina").Value.ToString()));
                case "pozajmljeno":
                    return Funkcije.EksponatFunkcije.kreiraj_pozajmljeno(eksponat.Tip, jEks.Property("nazivKolekcije").Value.ToString(), jEks.Property("adresa").Value.ToString(),
                        jEks.Property("opis").Value.ToString(), (jEks.Property("isPrivateOwner").Value.ToString() == "Da") ? true : false, 
                        eksponat.TipEksponata, idUmet);
                case "ostalo":
                    return Funkcije.EksponatFunkcije.kreiraj_eksponat(eksponat.TipEksponata, eksponat.Tip, false , idUmet);
		        default:  return false;
              
	        }
            return false;
        }

        public int RemoveEksponat(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Eksponat eks = s.Load<Eksponat>(id);

                s.Delete(eks);

                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }

        }

        public int RemoveUmetnik(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Umetnik eks = s.Load<Umetnik>(id);

                s.Delete(eks);

                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }

        public int RemoveDonator(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Donator don = s.Load<Donator>(id);

                s.Delete(don);

                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }
        }
    }
}
