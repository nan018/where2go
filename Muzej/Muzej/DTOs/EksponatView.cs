﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Muzej.Entiteti;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace Muzej.DTOs
{
    [Serializable]
    [DataContract]
    public class EksponatView
    {
        [DataMember]
        public  int Id { get; protected set; }
         [DataMember]
        public  String IsPozajmljen { get; set; }
         [DataMember]
        public  String Tip { get; set; }
         [DataMember]
        public  String TipEksponata { get; set; }

         [DataMember]
         public Umetnik Umetnik { get; set; }
        public EksponatView(Eksponat e)
        {
            this.Id = e.Id;
            this.IsPozajmljen = e.IsPozajmljen;
            this.Tip = e.Tip;
            this.TipEksponata = e.TipEksponata;
            if (e.Umetnik != null)
                Umetnik = e.Umetnik;
        }

        public EksponatView()
        {

        }
    }
}
