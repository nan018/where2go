﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Muzej.Entiteti;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using Muzej.Funkcije;
namespace Muzej
{
    public partial class UmetnikForm : Form
    {
        public UmetnikForm()
        {
            InitializeComponent();
        }
        Umetnik pikaso = null;
        public UmetnikForm(Umetnik pikaso)
        {
            InitializeComponent();
            this.pikaso = pikaso;
        
        } 
        
        private void chcDatumSmrti_CheckedChanged(object sender, EventArgs e)
        {
            if (chcDatumSmrti.Checked) dateTimePicker2.Enabled = true;
            else dateTimePicker2.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime? smrt = dateTimePicker2.Value;
            if (!dateTimePicker2.Enabled) smrt = null;

            if (UmetnikFunkcije.kreiraj_umetnika(dateTimePicker1.Value, smrt, txtIme.Text, txtZemlja.Text, txtEpoha.Text, txtStil.Text, txtTehnika.Text))
                MessageBox.Show("Uspesno kreiran umetnik");

        }



    }
}
