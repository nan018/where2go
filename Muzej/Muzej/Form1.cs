﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using Muzej.Entiteti;
using Muzej.Funkcije;

namespace Muzej
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Umetnik pikaso = s.Load<Muzej.Entiteti.Umetnik>(2);
                MessageBox.Show("Datum rodjenja: " + pikaso.DatumRodjenja + "\n" + "Datum smrti: " + pikaso.DatumSmrti + "\n" + "Epoha: "
                    + pikaso.Epoha + "\n" + "Glavna tehnika: " + pikaso.GlavnaTehnika + "\n" + "Glavni stil: " + pikaso.GlavniStil  +"\n" + "Zemlja porekla: " + pikaso.ZemljaPorekla);
                s.Close();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Eksponat eks = s.Load<Eksponat>(114);
                MessageBox.Show(" " + eks.Id + "\n" + " " + eks.IsPozajmljen + "\n" + " " + eks.Tip + "\n" + " " + eks.TipEksponata + "\n" + " " + eks.Umetnik.Ime);
                s.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            { 
                ISession s = DataLayer.GetSession();
                Pozajmljeno poz = s.Load<Pozajmljeno>(3);
                MessageBox.Show(poz.NazivKolekcije + "\n" + poz.Adresa+ "\n" + poz.Opis + "\n" + poz.TipEksponata);
                s.Close();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            ISession s = DataLayer.GetSession();
            Donira donira = s.Load<Donira>(40);
			
            MessageBox.Show(donira.Iznos + "\n" + donira.Eksponat.Tip);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            ISession s = DataLayer.GetSession();
            Donator don = s.Load<Privatno>(114);
            MessageBox.Show(don.Ime + "\n" + don.Adresa + "\n" + don.Telefon + "\n");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            ISession s = DataLayer.GetSession();
            Firma f = s.Load<Firma>(2);
            MessageBox.Show(f.Ime + "\n" + f.Adresa + "\n" + f.Telefon + "\n" + f.OblastPoslovanja);
        
        }

		private void UcitajIzlozbu_Click(object sender, EventArgs e)
		{
			ISession s = DataLayer.GetSession();
			Izlozba iz = s.Load<Izlozba>(1);
			MessageBox.Show(iz.Ime + "\n" + "Datum pocetka: " + iz.DatumPocetka.ToShortDateString() +
				"\n" + "Datum zavrsetka " + iz.DatumZavrsetka.ToShortDateString());
        }

        private void button8_Click(object sender, EventArgs e)
        {
			if(cmbTip.SelectedItem.ToString() == "slika")
				EksponatFunkcije.kreiraj_sliku(cmbTipEksp.Items[cmbTipEksp.SelectedIndex].ToString(), cmbTip.SelectedItem.ToString(), isPozajmljen.Checked, Int32.Parse(txtUmetnikId.Text), cmbTehnika.Items[cmbTehnika.SelectedIndex].ToString(), txtBoxStil.Text.ToString(), checkOriginal.Checked, txtBoxMaterijal.Text.ToString(), Convert.ToDecimal(txtBoxDuzina.Text.ToString()), Convert.ToDecimal(txtBoxSirina.Text.ToString()));
			else if (cmbTip.SelectedItem.ToString() == "statua")
				EksponatFunkcije.kreiraj_statuu(cmbTipEksp.Items[cmbTipEksp.SelectedIndex].ToString(), cmbTip.SelectedItem.ToString(), isPozajmljen.Checked, Int32.Parse(txtUmetnikId.Text), txtBoxStilStat.Text.ToString(), Convert.ToDecimal(txtBoxTezinaStat.Text), cmbMaterijal.Items[cmbMaterijal.SelectedIndex].ToString(), Convert.ToDecimal(txtBoxVisinaStat.Text.ToString()));
			else
				EksponatFunkcije.kreiraj_eksponat(cmbTipEksp.Items[cmbTipEksp.SelectedIndex].ToString(), cmbTip.SelectedItem.ToString(), isPozajmljen.Checked, Int32.Parse(txtUmetnikId.Text));
		}

        private void button7_Click(object sender, EventArgs e)
        {
           
            UmetnikForm forma = new UmetnikForm();
            forma.Show();

        }

        private void button9_Click(object sender, EventArgs e)
        {
          
            if (EksponatFunkcije.kreiraj_pozajmljeno("pozajmljeno", "Vizantijska", "Prvomajska 33", "Maketa Konstantinopolja", false, "predmet", 38))
                MessageBox.Show("Uspesno kreiran pozajmljenog eksponata");
            else MessageBox.Show("Greska prilikom kreiranja pozajmljenog eksponata");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (EksponatFunkcije.izbrisi_pozajmljeno(Int32.Parse(txtIdBrisanje.Text)))
                MessageBox.Show("Uspesno obrisan pozajmljeni eksponat");
            else MessageBox.Show("Greska prilikom brisanja pozajmljenog eksponata");
        }

        private void button11_Click(object sender, EventArgs e)
        {
           /* ISession s = DataLayer.GetSession();

            Donator stari = s.Load<Donator>(Int32.Parse(txtIdDonatora.Text));

            s.Delete(stari);

            s.Flush();

            s.Close();*/
            if (DonatorFunkcije.obrisi_donatora(Int32.Parse(txtIdDonatora.Text),true))
                MessageBox.Show("Uspesno obrisan donator");
            else MessageBox.Show("Greska prilikom brisanja donatora");
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (UmetnikFunkcije.obrisi_umetnika(Int32.Parse(txtIdUmetnika.Text)))
                MessageBox.Show("Uspesno obrisan umetnik");
            else MessageBox.Show("Greska prilikom brisanja umetnika");
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (UmetnikFunkcije.izmeni_umetnika(38,"Renesansa","Akvarel", "Mozaik"))
                MessageBox.Show("Uspesno izmenjen umetnik");
            else MessageBox.Show("Greska prilikom izmene umetnika");
        }

        private void button14_Click(object sender, EventArgs e)
        {

            

           if (DonatorFunkcije.kreiraj_donatora("Nikole Pasica 51", "065559333", "Radmilo", "Knjigovodstvo", false))
                MessageBox.Show("Uspesno kreiran donator");
            else MessageBox.Show("Greska prilikom kreiranja donatora");
            
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (DonatorFunkcije.doniraj(102,10,(decimal)7000,DateTime.Now))
                MessageBox.Show("Uspesno kreirana donacija");
            else MessageBox.Show("Greska prilikom kreiranja donacije");
        }

        private void button16_Click(object sender, EventArgs e)
        {
            if(DonatorFunkcije.izmeni_donatora(100, "Veternicka bb", "068528712", "Milan", "Da"))
                MessageBox.Show("Uspesno izmenjen donator");
            else MessageBox.Show("Greska prilikom izmene donatora");
        }

        private void button17_Click(object sender, EventArgs e)
        {
            if (DonatorFunkcije.obrisi_donaciju(Int32.Parse(txtIdDonacije.Text)))
                MessageBox.Show("Uspesno obrisana donacija");
            else MessageBox.Show("Greska prilikom brisanja donacije");
        }

        private void button18_Click(object sender, EventArgs e)
        {
            if (EksponatFunkcije.izmeni_pozajmljeno(62,"pozajmljeno","Porodicna", "Gavrila Principa 16","Fotografija Obrenovica",true,"fotografija",true,32))
                MessageBox.Show("Uspesno izmenjen pozajmljeni eksponat");
            else MessageBox.Show("Greska prilikom izmene pozajmljenog eksponata");
        }

        private void button19_Click(object sender, EventArgs e)
        {
            if (EksponatFunkcije.izmeni_eksponat(52, "ostalo","stampani materijal",6))
                MessageBox.Show("Uspesno izmenjen eksponat");
            else MessageBox.Show("Greska prilikom izmene eksponata");
        }

        private void Button20_Click(object sender, EventArgs e)
        {
            EksponatiInformacije ei = new EksponatiInformacije();
            ei.ShowDialog();
        }
    }
}
