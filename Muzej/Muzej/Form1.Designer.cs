﻿namespace Muzej
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.UcitajIzlozbu = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.label13 = new System.Windows.Forms.Label();
			this.txtBoxVisinaStat = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.txtBoxTezinaStat = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.txtBoxStilStat = new System.Windows.Forms.TextBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.checkOriginal = new System.Windows.Forms.CheckBox();
			this.label10 = new System.Windows.Forms.Label();
			this.txtBoxSirina = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.txtBoxDuzina = new System.Windows.Forms.TextBox();
			this.label8 = new System.Windows.Forms.Label();
			this.txtBoxMaterijal = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.txtBoxStil = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.txtUmetnikId = new System.Windows.Forms.MaskedTextBox();
			this.cmbTipEksp = new System.Windows.Forms.ComboBox();
			this.cmbTip = new System.Windows.Forms.ComboBox();
			this.isPozajmljen = new System.Windows.Forms.CheckBox();
			this.label1 = new System.Windows.Forms.Label();
			this.button8 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.button10 = new System.Windows.Forms.Button();
			this.txtIdBrisanje = new System.Windows.Forms.MaskedTextBox();
			this.button11 = new System.Windows.Forms.Button();
			this.txtIdDonatora = new System.Windows.Forms.MaskedTextBox();
			this.button12 = new System.Windows.Forms.Button();
			this.button13 = new System.Windows.Forms.Button();
			this.txtIdUmetnika = new System.Windows.Forms.MaskedTextBox();
			this.button14 = new System.Windows.Forms.Button();
			this.button15 = new System.Windows.Forms.Button();
			this.button16 = new System.Windows.Forms.Button();
			this.Delete = new System.Windows.Forms.GroupBox();
			this.txtIdDonacije = new System.Windows.Forms.MaskedTextBox();
			this.button17 = new System.Windows.Forms.Button();
			this.Create = new System.Windows.Forms.GroupBox();
			this.Update = new System.Windows.Forms.GroupBox();
			this.button19 = new System.Windows.Forms.Button();
			this.button18 = new System.Windows.Forms.Button();
			this.button20 = new System.Windows.Forms.Button();
			this.cmbTehnika = new System.Windows.Forms.ComboBox();
			this.cmbMaterijal = new System.Windows.Forms.ComboBox();
			this.groupBox1.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.Delete.SuspendLayout();
			this.Create.SuspendLayout();
			this.Update.SuspendLayout();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(16, 26);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(105, 32);
			this.button1.TabIndex = 0;
			this.button1.Text = "Ucitaj umetnika";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(16, 74);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(105, 32);
			this.button2.TabIndex = 3;
			this.button2.Text = "Ucitaj eksponat";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(16, 123);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(105, 41);
			this.button3.TabIndex = 4;
			this.button3.Text = "Ucitaj pozajmljen eksponat";
			this.button3.UseVisualStyleBackColor = true;
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(16, 183);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(105, 35);
			this.button4.TabIndex = 5;
			this.button4.Text = "Ucitaj donaciju";
			this.button4.UseVisualStyleBackColor = true;
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(16, 234);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(105, 36);
			this.button5.TabIndex = 6;
			this.button5.Text = "Ucitaj donatora";
			this.button5.UseVisualStyleBackColor = true;
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(16, 290);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(105, 36);
			this.button6.TabIndex = 7;
			this.button6.Text = "Ucitaj firmu donatora";
			this.button6.UseVisualStyleBackColor = true;
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// UcitajIzlozbu
			// 
			this.UcitajIzlozbu.Location = new System.Drawing.Point(16, 342);
			this.UcitajIzlozbu.Name = "UcitajIzlozbu";
			this.UcitajIzlozbu.Size = new System.Drawing.Size(105, 36);
			this.UcitajIzlozbu.TabIndex = 8;
			this.UcitajIzlozbu.Text = "Ucitaj izlozbu";
			this.UcitajIzlozbu.UseVisualStyleBackColor = true;
			this.UcitajIzlozbu.Click += new System.EventHandler(this.UcitajIzlozbu_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.groupBox4);
			this.groupBox1.Controls.Add(this.groupBox3);
			this.groupBox1.Controls.Add(this.groupBox2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.button8);
			this.groupBox1.Location = new System.Drawing.Point(149, 26);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(457, 352);
			this.groupBox1.TabIndex = 19;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Kreiraj Eksponat";
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.cmbMaterijal);
			this.groupBox4.Controls.Add(this.label13);
			this.groupBox4.Controls.Add(this.txtBoxVisinaStat);
			this.groupBox4.Controls.Add(this.label12);
			this.groupBox4.Controls.Add(this.label11);
			this.groupBox4.Controls.Add(this.txtBoxTezinaStat);
			this.groupBox4.Controls.Add(this.label4);
			this.groupBox4.Controls.Add(this.txtBoxStilStat);
			this.groupBox4.ForeColor = System.Drawing.Color.Red;
			this.groupBox4.Location = new System.Drawing.Point(9, 169);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(207, 166);
			this.groupBox4.TabIndex = 40;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Ukoliko dodajete statuu unesite";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(154, 129);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(35, 13);
			this.label13.TabIndex = 7;
			this.label13.Text = "Visina";
			// 
			// txtBoxVisinaStat
			// 
			this.txtBoxVisinaStat.Location = new System.Drawing.Point(10, 126);
			this.txtBoxVisinaStat.Name = "txtBoxVisinaStat";
			this.txtBoxVisinaStat.Size = new System.Drawing.Size(138, 20);
			this.txtBoxVisinaStat.TabIndex = 6;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(154, 98);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(46, 13);
			this.label12.TabIndex = 5;
			this.label12.Text = "Materijal";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(154, 64);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(39, 13);
			this.label11.TabIndex = 3;
			this.label11.Text = "Tezina";
			// 
			// txtBoxTezinaStat
			// 
			this.txtBoxTezinaStat.Location = new System.Drawing.Point(10, 61);
			this.txtBoxTezinaStat.Name = "txtBoxTezinaStat";
			this.txtBoxTezinaStat.Size = new System.Drawing.Size(138, 20);
			this.txtBoxTezinaStat.TabIndex = 2;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(156, 30);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(21, 13);
			this.label4.TabIndex = 1;
			this.label4.Text = "Stil";
			// 
			// txtBoxStilStat
			// 
			this.txtBoxStilStat.Location = new System.Drawing.Point(10, 27);
			this.txtBoxStilStat.Name = "txtBoxStilStat";
			this.txtBoxStilStat.Size = new System.Drawing.Size(138, 20);
			this.txtBoxStilStat.TabIndex = 0;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.cmbTehnika);
			this.groupBox3.Controls.Add(this.checkOriginal);
			this.groupBox3.Controls.Add(this.label10);
			this.groupBox3.Controls.Add(this.txtBoxSirina);
			this.groupBox3.Controls.Add(this.label9);
			this.groupBox3.Controls.Add(this.txtBoxDuzina);
			this.groupBox3.Controls.Add(this.label8);
			this.groupBox3.Controls.Add(this.txtBoxMaterijal);
			this.groupBox3.Controls.Add(this.label6);
			this.groupBox3.Controls.Add(this.txtBoxStil);
			this.groupBox3.Controls.Add(this.label5);
			this.groupBox3.ForeColor = System.Drawing.Color.Red;
			this.groupBox3.Location = new System.Drawing.Point(227, 25);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(230, 229);
			this.groupBox3.TabIndex = 39;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Ukoliko dodajete sliku unesite";
			// 
			// checkOriginal
			// 
			this.checkOriginal.AutoSize = true;
			this.checkOriginal.Location = new System.Drawing.Point(22, 85);
			this.checkOriginal.Name = "checkOriginal";
			this.checkOriginal.Size = new System.Drawing.Size(81, 17);
			this.checkOriginal.TabIndex = 38;
			this.checkOriginal.Text = "Original ram";
			this.checkOriginal.UseVisualStyleBackColor = true;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(146, 193);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(33, 13);
			this.label10.TabIndex = 37;
			this.label10.Text = "Sirina";
			// 
			// txtBoxSirina
			// 
			this.txtBoxSirina.Location = new System.Drawing.Point(22, 190);
			this.txtBoxSirina.Name = "txtBoxSirina";
			this.txtBoxSirina.Size = new System.Drawing.Size(112, 20);
			this.txtBoxSirina.TabIndex = 36;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(146, 154);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(40, 13);
			this.label9.TabIndex = 35;
			this.label9.Text = "Duzina";
			// 
			// txtBoxDuzina
			// 
			this.txtBoxDuzina.Location = new System.Drawing.Point(22, 151);
			this.txtBoxDuzina.Name = "txtBoxDuzina";
			this.txtBoxDuzina.Size = new System.Drawing.Size(112, 20);
			this.txtBoxDuzina.TabIndex = 34;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(146, 121);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(46, 13);
			this.label8.TabIndex = 33;
			this.label8.Text = "Materijal";
			// 
			// txtBoxMaterijal
			// 
			this.txtBoxMaterijal.Location = new System.Drawing.Point(22, 118);
			this.txtBoxMaterijal.Name = "txtBoxMaterijal";
			this.txtBoxMaterijal.Size = new System.Drawing.Size(112, 20);
			this.txtBoxMaterijal.TabIndex = 32;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(146, 49);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(21, 13);
			this.label6.TabIndex = 29;
			this.label6.Text = "Stil";
			// 
			// txtBoxStil
			// 
			this.txtBoxStil.Location = new System.Drawing.Point(22, 46);
			this.txtBoxStil.Name = "txtBoxStil";
			this.txtBoxStil.Size = new System.Drawing.Size(112, 20);
			this.txtBoxStil.TabIndex = 28;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(149, 18);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(46, 13);
			this.label5.TabIndex = 27;
			this.label5.Text = "Tehnika";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Controls.Add(this.txtUmetnikId);
			this.groupBox2.Controls.Add(this.cmbTipEksp);
			this.groupBox2.Controls.Add(this.cmbTip);
			this.groupBox2.Controls.Add(this.isPozajmljen);
			this.groupBox2.Location = new System.Drawing.Point(6, 25);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(211, 135);
			this.groupBox2.TabIndex = 38;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Unos osnovnih podataka";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(137, 103);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(64, 13);
			this.label3.TabIndex = 24;
			this.label3.Text = "ID umetnika";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(137, 43);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(22, 13);
			this.label2.TabIndex = 23;
			this.label2.Text = "Tip";
			// 
			// txtUmetnikId
			// 
			this.txtUmetnikId.Location = new System.Drawing.Point(7, 100);
			this.txtUmetnikId.Mask = "00000";
			this.txtUmetnikId.Name = "txtUmetnikId";
			this.txtUmetnikId.Size = new System.Drawing.Size(124, 20);
			this.txtUmetnikId.TabIndex = 21;
			this.txtUmetnikId.ValidatingType = typeof(int);
			// 
			// cmbTipEksp
			// 
			this.cmbTipEksp.FormattingEnabled = true;
			this.cmbTipEksp.Items.AddRange(new object[] {
            "fotografija",
            "stampani materijal",
            "predmet"});
			this.cmbTipEksp.Location = new System.Drawing.Point(7, 13);
			this.cmbTipEksp.Name = "cmbTipEksp";
			this.cmbTipEksp.Size = new System.Drawing.Size(124, 21);
			this.cmbTipEksp.TabIndex = 20;
			// 
			// cmbTip
			// 
			this.cmbTip.FormattingEnabled = true;
			this.cmbTip.Items.AddRange(new object[] {
            "slika",
            "statua",
            "pozajmljeno",
            "ostalo"});
			this.cmbTip.Location = new System.Drawing.Point(7, 40);
			this.cmbTip.Name = "cmbTip";
			this.cmbTip.Size = new System.Drawing.Size(124, 21);
			this.cmbTip.TabIndex = 19;
			// 
			// isPozajmljen
			// 
			this.isPozajmljen.AutoSize = true;
			this.isPozajmljen.Location = new System.Drawing.Point(13, 67);
			this.isPozajmljen.Name = "isPozajmljen";
			this.isPozajmljen.Size = new System.Drawing.Size(83, 17);
			this.isPozajmljen.TabIndex = 0;
			this.isPozajmljen.Text = "isPozajmljen";
			this.isPozajmljen.UseVisualStyleBackColor = true;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(146, 41);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(75, 13);
			this.label1.TabIndex = 22;
			this.label1.Text = "Tip eksponata";
			// 
			// button8
			// 
			this.button8.Location = new System.Drawing.Point(227, 267);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(214, 68);
			this.button8.TabIndex = 21;
			this.button8.Text = "Kreiraj eksponat";
			this.button8.UseVisualStyleBackColor = true;
			this.button8.Click += new System.EventHandler(this.button8_Click);
			// 
			// button7
			// 
			this.button7.Location = new System.Drawing.Point(21, 26);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(113, 29);
			this.button7.TabIndex = 22;
			this.button7.Text = "Kreiraj umetnika";
			this.button7.UseVisualStyleBackColor = true;
			this.button7.Click += new System.EventHandler(this.button7_Click);
			// 
			// button9
			// 
			this.button9.Location = new System.Drawing.Point(156, 18);
			this.button9.Name = "button9";
			this.button9.Size = new System.Drawing.Size(114, 44);
			this.button9.TabIndex = 23;
			this.button9.Text = "Kreiraj pozajmljen eksponat";
			this.button9.UseVisualStyleBackColor = true;
			this.button9.Click += new System.EventHandler(this.button9_Click);
			// 
			// button10
			// 
			this.button10.Location = new System.Drawing.Point(50, 31);
			this.button10.Name = "button10";
			this.button10.Size = new System.Drawing.Size(103, 41);
			this.button10.TabIndex = 24;
			this.button10.Text = "Obrisi pozajmljen eksponat";
			this.button10.UseVisualStyleBackColor = true;
			this.button10.Click += new System.EventHandler(this.button10_Click);
			// 
			// txtIdBrisanje
			// 
			this.txtIdBrisanje.Location = new System.Drawing.Point(11, 42);
			this.txtIdBrisanje.Mask = "00000";
			this.txtIdBrisanje.Name = "txtIdBrisanje";
			this.txtIdBrisanje.Size = new System.Drawing.Size(33, 20);
			this.txtIdBrisanje.TabIndex = 26;
			this.txtIdBrisanje.ValidatingType = typeof(int);
			// 
			// button11
			// 
			this.button11.Location = new System.Drawing.Point(49, 90);
			this.button11.Name = "button11";
			this.button11.Size = new System.Drawing.Size(103, 35);
			this.button11.TabIndex = 27;
			this.button11.Text = "Obrisi donatora";
			this.button11.UseVisualStyleBackColor = true;
			this.button11.Click += new System.EventHandler(this.button11_Click);
			// 
			// txtIdDonatora
			// 
			this.txtIdDonatora.Location = new System.Drawing.Point(11, 98);
			this.txtIdDonatora.Mask = "00000";
			this.txtIdDonatora.Name = "txtIdDonatora";
			this.txtIdDonatora.Size = new System.Drawing.Size(33, 20);
			this.txtIdDonatora.TabIndex = 28;
			this.txtIdDonatora.ValidatingType = typeof(int);
			// 
			// button12
			// 
			this.button12.Location = new System.Drawing.Point(23, 25);
			this.button12.Name = "button12";
			this.button12.Size = new System.Drawing.Size(113, 36);
			this.button12.TabIndex = 29;
			this.button12.Text = "Izmeni umetnika";
			this.button12.UseVisualStyleBackColor = true;
			this.button12.Click += new System.EventHandler(this.button12_Click);
			// 
			// button13
			// 
			this.button13.Location = new System.Drawing.Point(50, 198);
			this.button13.Name = "button13";
			this.button13.Size = new System.Drawing.Size(103, 36);
			this.button13.TabIndex = 30;
			this.button13.Text = "Obrisi umetnika";
			this.button13.UseVisualStyleBackColor = true;
			this.button13.Click += new System.EventHandler(this.button13_Click);
			// 
			// txtIdUmetnika
			// 
			this.txtIdUmetnika.Location = new System.Drawing.Point(11, 207);
			this.txtIdUmetnika.Mask = "00000";
			this.txtIdUmetnika.Name = "txtIdUmetnika";
			this.txtIdUmetnika.Size = new System.Drawing.Size(33, 20);
			this.txtIdUmetnika.TabIndex = 31;
			this.txtIdUmetnika.ValidatingType = typeof(int);
			// 
			// button14
			// 
			this.button14.Location = new System.Drawing.Point(299, 22);
			this.button14.Name = "button14";
			this.button14.Size = new System.Drawing.Size(114, 36);
			this.button14.TabIndex = 32;
			this.button14.Text = "Kreiraj donatora";
			this.button14.UseVisualStyleBackColor = true;
			this.button14.Click += new System.EventHandler(this.button14_Click);
			// 
			// button15
			// 
			this.button15.Location = new System.Drawing.Point(439, 22);
			this.button15.Name = "button15";
			this.button15.Size = new System.Drawing.Size(113, 36);
			this.button15.TabIndex = 33;
			this.button15.Text = "Kreiraj donaciju";
			this.button15.UseVisualStyleBackColor = true;
			this.button15.Click += new System.EventHandler(this.button15_Click);
			// 
			// button16
			// 
			this.button16.Location = new System.Drawing.Point(23, 81);
			this.button16.Name = "button16";
			this.button16.Size = new System.Drawing.Size(113, 38);
			this.button16.TabIndex = 34;
			this.button16.Text = "Izmeni donatora";
			this.button16.UseVisualStyleBackColor = true;
			this.button16.Click += new System.EventHandler(this.button16_Click);
			// 
			// Delete
			// 
			this.Delete.Controls.Add(this.txtIdDonacije);
			this.Delete.Controls.Add(this.button17);
			this.Delete.Controls.Add(this.button10);
			this.Delete.Controls.Add(this.txtIdBrisanje);
			this.Delete.Controls.Add(this.button11);
			this.Delete.Controls.Add(this.txtIdDonatora);
			this.Delete.Controls.Add(this.txtIdUmetnika);
			this.Delete.Controls.Add(this.button13);
			this.Delete.Location = new System.Drawing.Point(612, 290);
			this.Delete.Name = "Delete";
			this.Delete.Size = new System.Drawing.Size(158, 244);
			this.Delete.TabIndex = 35;
			this.Delete.TabStop = false;
			this.Delete.Text = "Delete";
			// 
			// txtIdDonacije
			// 
			this.txtIdDonacije.Location = new System.Drawing.Point(11, 152);
			this.txtIdDonacije.Mask = "00000";
			this.txtIdDonacije.Name = "txtIdDonacije";
			this.txtIdDonacije.Size = new System.Drawing.Size(33, 20);
			this.txtIdDonacije.TabIndex = 33;
			this.txtIdDonacije.ValidatingType = typeof(int);
			// 
			// button17
			// 
			this.button17.Location = new System.Drawing.Point(50, 143);
			this.button17.Name = "button17";
			this.button17.Size = new System.Drawing.Size(104, 36);
			this.button17.TabIndex = 32;
			this.button17.Text = "Obrisi donaciju";
			this.button17.UseVisualStyleBackColor = true;
			this.button17.Click += new System.EventHandler(this.button17_Click);
			// 
			// Create
			// 
			this.Create.Controls.Add(this.button15);
			this.Create.Controls.Add(this.button7);
			this.Create.Controls.Add(this.button14);
			this.Create.Controls.Add(this.button9);
			this.Create.Location = new System.Drawing.Point(16, 388);
			this.Create.Name = "Create";
			this.Create.Size = new System.Drawing.Size(590, 81);
			this.Create.TabIndex = 36;
			this.Create.TabStop = false;
			this.Create.Text = "Create";
			// 
			// Update
			// 
			this.Update.Controls.Add(this.button19);
			this.Update.Controls.Add(this.button18);
			this.Update.Controls.Add(this.button12);
			this.Update.Controls.Add(this.button16);
			this.Update.Location = new System.Drawing.Point(612, 26);
			this.Update.Name = "Update";
			this.Update.Size = new System.Drawing.Size(158, 246);
			this.Update.TabIndex = 37;
			this.Update.TabStop = false;
			this.Update.Text = "Update";
			// 
			// button19
			// 
			this.button19.Location = new System.Drawing.Point(23, 196);
			this.button19.Name = "button19";
			this.button19.Size = new System.Drawing.Size(113, 35);
			this.button19.TabIndex = 36;
			this.button19.Text = "Izmeni eksponat";
			this.button19.UseVisualStyleBackColor = true;
			this.button19.Click += new System.EventHandler(this.button19_Click);
			// 
			// button18
			// 
			this.button18.Location = new System.Drawing.Point(23, 132);
			this.button18.Name = "button18";
			this.button18.Size = new System.Drawing.Size(113, 46);
			this.button18.TabIndex = 35;
			this.button18.Text = "Izmeni pozajmljen eksponat";
			this.button18.UseVisualStyleBackColor = true;
			this.button18.Click += new System.EventHandler(this.button18_Click);
			// 
			// button20
			// 
			this.button20.Location = new System.Drawing.Point(16, 480);
			this.button20.Margin = new System.Windows.Forms.Padding(2);
			this.button20.Name = "button20";
			this.button20.Size = new System.Drawing.Size(590, 44);
			this.button20.TabIndex = 38;
			this.button20.Text = "Pregled eksponata";
			this.button20.UseVisualStyleBackColor = true;
			this.button20.Click += new System.EventHandler(this.Button20_Click);
			// 
			// cmbTehnika
			// 
			this.cmbTehnika.FormattingEnabled = true;
			this.cmbTehnika.Items.AddRange(new object[] {
            "ulje",
            "akvarel",
            "vostane boje",
            "drvene boje"});
			this.cmbTehnika.Location = new System.Drawing.Point(22, 15);
			this.cmbTehnika.Name = "cmbTehnika";
			this.cmbTehnika.Size = new System.Drawing.Size(121, 21);
			this.cmbTehnika.TabIndex = 39;
			// 
			// cmbMaterijal
			// 
			this.cmbMaterijal.FormattingEnabled = true;
			this.cmbMaterijal.Items.AddRange(new object[] {
            "drvo",
            "kamen",
            "gips",
            "bronza"});
			this.cmbMaterijal.Location = new System.Drawing.Point(10, 95);
			this.cmbMaterijal.Name = "cmbMaterijal";
			this.cmbMaterijal.Size = new System.Drawing.Size(138, 21);
			this.cmbMaterijal.TabIndex = 8;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(808, 546);
			this.Controls.Add(this.button20);
			this.Controls.Add(this.Update);
			this.Controls.Add(this.Create);
			this.Controls.Add(this.Delete);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.UcitajIzlozbu);
			this.Controls.Add(this.button6);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.groupBox3.ResumeLayout(false);
			this.groupBox3.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.Delete.ResumeLayout(false);
			this.Delete.PerformLayout();
			this.Create.ResumeLayout(false);
			this.Update.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button UcitajIzlozbu;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox txtUmetnikId;
        private System.Windows.Forms.ComboBox cmbTipEksp;
        private System.Windows.Forms.ComboBox cmbTip;
        private System.Windows.Forms.CheckBox isPozajmljen;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.MaskedTextBox txtIdBrisanje;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.MaskedTextBox txtIdDonatora;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.MaskedTextBox txtIdUmetnika;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.GroupBox Delete;
        private System.Windows.Forms.GroupBox Create;
        private System.Windows.Forms.MaskedTextBox txtIdDonacije;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.GroupBox Update;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.TextBox txtBoxVisinaStat;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.TextBox txtBoxTezinaStat;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox txtBoxStilStat;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.TextBox txtBoxSirina;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox txtBoxDuzina;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox txtBoxMaterijal;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox txtBoxStil;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.CheckBox checkOriginal;
		private System.Windows.Forms.ComboBox cmbMaterijal;
		private System.Windows.Forms.ComboBox cmbTehnika;
	}
}

