﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Muzej.Entiteti;
using FluentNHibernate.Mapping;

namespace muzej.mapiranja
{
	class Izlozbamapiranja : ClassMap<Izlozba>
	{
		public Izlozbamapiranja()
		{
			Table("IZLOZBA");

			Id(x => x.Id).Column("ID").GeneratedBy.SequenceIdentity("IZLOZBA_ID_SEQ");

			Map(x => x.Ime, "IME");
			Map(x => x.DatumPocetka, "DATUMPOCETKA");
			Map(x => x.DatumZavrsetka, "DATUMZAVRSETKA");

			HasMany(x => x.IzlozbaEksponata).KeyColumn("IDIZLOZBE").LazyLoad().Inverse().Cascade.All();
		}
	}
}
