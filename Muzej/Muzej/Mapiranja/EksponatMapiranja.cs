﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Muzej.Entiteti;

namespace Muzej.Mapiranja
{
    public class EksponatMapiranja : ClassMap<Muzej.Entiteti.Eksponat>
    {
		
        public EksponatMapiranja()
        {
            Table("EKSPONAT");

           
            Id(x => x.Id, "ID").GeneratedBy.SequenceIdentity("EKSPONAT_ID_SEQ");
            Map(x => x.IsPozajmljen, "ISPOZAJMLJEN");
            Map(x => x.Tip, "TIP");
            Map(x => x.TipEksponata, "TIPEKSPONATA");

            References(x => x.Umetnik).Column("ID_UMETNIKA").Cascade.None();//ovde bilo All

            HasMany(x => x.DonatorDonirao).KeyColumn("IDEKSPONATA").LazyLoad().Inverse().Cascade.All();
            HasMany(x => x.IzlozenNaIzlozbe).KeyColumn("IDEKSPONATA").LazyLoad().Inverse().Cascade.All();
        }

    }
    
       
           
}
