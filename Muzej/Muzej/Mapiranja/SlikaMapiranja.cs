﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Muzej.Entiteti;
using FluentNHibernate.Mapping;

namespace Muzej.Mapiranja
{
	 public class SlikaMapiranja : SubclassMap<Slika>
	{
		public SlikaMapiranja()
		{
			Table("SLIKA");

			KeyColumn("ID");

			Map(x => x.Tehnika, "TEHNIKA");
			Map(x => x.Stil, "STIL");
			Map(x => x.OriginalRam, "ISORIGINALRAM");
			Map(x => x.Materijal, "MATERIJAL");
			Map(x => x.Duzina, "DUZINA");
			Map(x => x.Sirina, "SIRINA");
		}
	}
}
