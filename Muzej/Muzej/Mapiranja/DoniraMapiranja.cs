﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Muzej.Entiteti;

namespace Muzej.Mapiranja
{
    public class DoniraMapiranja : ClassMap<Donira>
    {
        public DoniraMapiranja()
        {
            Table("DONIRA");

            Id(x => x.Id, "ID").GeneratedBy.SequenceIdentity("DONIRA_ID_SEQ");

            Map(x => x.Iznos, "IZNOS");
            Map(x => x.DatumDonacije, "DATUMDONACIJE");

            References(x => x.Donator, "IDDONATORA");
            References(x => x.Eksponat, "IDEKSPONATA");
        }
    }
}
