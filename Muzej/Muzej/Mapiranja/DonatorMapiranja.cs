﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Muzej.Entiteti;

namespace Muzej.Mapiranja
{
    public class DonatorMapiranja : ClassMap<Donator>
    {
        public DonatorMapiranja()
        {
            Table("DONATOR");

            DiscriminateSubClassesOnColumn("ISFIRMA", "Ne                    ");

            Id(x => x.Id, "ID").GeneratedBy.SequenceIdentity("DONATOR_ID_SEQ");

            Map(x => x.Adresa, "ADRESA");
            Map(x => x.Telefon, "TELEFON");
            Map(x => x.Ime, "IME");

            //Map(x => x.IsFirma, "ISFIRMA");
            Map(x => x.OblastPoslovanja, "OBLASTPOSLOVANJA");

            HasMany(x => x.DoniraEksponat).KeyColumn("IDDONATORA").LazyLoad().Inverse().Cascade.All();

        }
    
    }
    public class FirmaMapiranja : SubclassMap<Firma>
    {
        public FirmaMapiranja()
        {
            DiscriminatorValue("Da                    "); 
        }
    }
    public class PrivatnoMapiranja : SubclassMap<Privatno>
    {
        public PrivatnoMapiranja()
        {
            DiscriminatorValue("Ne                    ");
        }
    }
    
}
