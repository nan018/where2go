﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Muzej.Entiteti;
using FluentNHibernate.Mapping;

namespace Muzej.Mapiranja
{
	class StatuaMapiranja : SubclassMap<Statua>
	{
		public StatuaMapiranja()
		{
			Table("STATUA");

			KeyColumn("ID");

			Map(x => x.Stil, "STIL");
			Map(x => x.Tezina, "TEZINA");
			Map(x => x.Materijal, "MATERIJAL");
			Map(x => x.Visina, "VISINA");
		}
	}
}
