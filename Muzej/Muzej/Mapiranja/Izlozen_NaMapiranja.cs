﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Muzej.Entiteti;

namespace Muzej.Mapiranja
{
	class Izlozen_NaMapiranja : ClassMap<Izlozen_Na>
	{
		public Izlozen_NaMapiranja()
		{
			Table("IZLOZEN_NA");

			Id(x => x.Id, "ID").GeneratedBy.SequenceIdentity("IZLOZEN_NA_ID_SEQ");

            References(x => x.OdrzavaSeIzlozba, "IDIZLOZBE"); 
			References(x => x.IzlazeSeEksponat, "IDEKSPONATA");

		}
	}
}