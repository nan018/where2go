﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;
using Muzej.Entiteti;

namespace Muzej.Mapiranja
{
    public class PozajmljenoMapiranja : SubclassMap<Pozajmljeno>
    {
        public PozajmljenoMapiranja()
        {

            Table("POZAJMLJENO");
            KeyColumn("ID");
            Map(x => x.NazivKolekcije, "NAZIVKOLEKCIJE");
            Map(x => x.Adresa, "ADRESA");
            Map(x => x.Opis, "OPIS");
            Map(x => x.IsPrivateOwner, "ISPRIVATEOWNER");
			
        }

    }
}
