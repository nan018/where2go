﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Muzej.Entiteti;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace Muzej.Mapiranja
{
    public class UmetnikMapiranja : ClassMap<Umetnik>
    {
        public UmetnikMapiranja()
        {
            Table("UMETNIK");

           // Id(x => x.Id, "ID").GeneratedBy.TriggerIdentity().UnsavedValue(-1);
            Id(x => x.Id, "ID").GeneratedBy.SequenceIdentity("UMETNIK_ID_SEQ");
            Map(x => x.DatumRodjenja, "DATUMRODJENJA");
            Map(x => x.DatumSmrti, "DATUMSMRTI");
            Map(x => x.Epoha, "EPOHA");
            Map(x => x.GlavnaTehnika, "GLAVNATEHNIKA");
            Map(x => x.GlavniStil, "GLAVNISTIL");
            Map(x => x.ZemljaPorekla, "ZEMLJAPOREKLA");
            Map(x => x.Ime, "IME");
            /*
             * HasMany is probably the most common collection-based relationship
             * you're going to use.
             * HasMany is the "other side" of a #References / many-to-one relationship,
             * and gets applied on the "one side" (one author has many books). 
             */
            HasMany(x => x.Eksponati).KeyColumn("ID_UMETNIKA").LazyLoad().Inverse().Cascade.SaveUpdate();
           


        }

    }
}
