﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Muzej
{
    public partial class EksponatiInformacije : Form
    {
        public EksponatiInformacije()
        {
            InitializeComponent();
        }

        public int EksponatId { get; set; }

        public EksponatiInformacije(int ekId)
        {
            this.EksponatId = ekId;

            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
			if (listView1.SelectedItems.Count == 0)
			{
				MessageBox.Show("Odaberite eksponat");
				return;
			}

			int odId = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);
			int umId = Int32.Parse(listView1.SelectedItems[0].SubItems[4].Text);
			EksponatPregled ep = DTOManager.GetEksponatPregled(odId,umId);

			EksponatiEdit edbForm = new EksponatiEdit(ep, this);
			if (edbForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{

			}
		}

        private void EksponatiInformacije_Load(object sender, EventArgs e)
        {
            this.PopulateInfos();
        }

        public void PopulateInfos()
        {
            listView1.Items.Clear();
            List<EksponatPregled> odInfos = DTOManager.GetOdInfos();
            foreach (EksponatPregled ep in odInfos)
            {
                if(!String.IsNullOrEmpty(ep.UmetnikId.ToString()) && !String.IsNullOrEmpty(ep.TipEksponata))
                {
                    ListViewItem item = new ListViewItem(new string[] { ep.EksponatId.ToString(), ep.Pozajmljen.ToString(), ep.Tip.ToString(), ep.TipEksponata.ToString(),ep.UmetnikId.ToString() });
                    listView1.Items.Add(item);
                }
                else
                {
                    ListViewItem item = new ListViewItem(new string[] { ep.EksponatId.ToString(), (ep.Pozajmljen!=null)? ep.Pozajmljen.ToString():"Ne", ep.Tip.ToString(), null, null });
                    listView1.Items.Add(item);
                }
            }
            listView1.Refresh();
        }
    }
}
