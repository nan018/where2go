﻿namespace Muzej
{
	partial class EksponatiEdit
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.pozEdit = new System.Windows.Forms.CheckBox();
			this.tipEdit = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tipEksEdit = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.idUmEdit = new System.Windows.Forms.MaskedTextBox();
			this.SuspendLayout();
			// 
			// pozEdit
			// 
			this.pozEdit.AutoSize = true;
			this.pozEdit.Location = new System.Drawing.Point(27, 23);
			this.pozEdit.Name = "pozEdit";
			this.pozEdit.Size = new System.Drawing.Size(76, 17);
			this.pozEdit.TabIndex = 0;
			this.pozEdit.Text = "Pozajmljen";
			this.pozEdit.UseVisualStyleBackColor = true;
			this.pozEdit.Click += new System.EventHandler(this.pozEdit_Click);
			// 
			// tipEdit
			// 
			this.tipEdit.FormattingEnabled = true;
			this.tipEdit.Items.AddRange(new object[] {
            "slika",
            "statua",
            "pozajmljeno",
            "ostalo"});
			this.tipEdit.Location = new System.Drawing.Point(27, 57);
			this.tipEdit.Name = "tipEdit";
			this.tipEdit.Size = new System.Drawing.Size(121, 21);
			this.tipEdit.TabIndex = 1;
			this.tipEdit.SelectedIndexChanged += new System.EventHandler(this.tipEdit_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(163, 60);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(22, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Tip";
			// 
			// tipEksEdit
			// 
			this.tipEksEdit.FormattingEnabled = true;
			this.tipEksEdit.Items.AddRange(new object[] {
            "fotografija",
            "stampani materijal",
            "predmet"});
			this.tipEksEdit.Location = new System.Drawing.Point(27, 103);
			this.tipEksEdit.Name = "tipEksEdit";
			this.tipEksEdit.Size = new System.Drawing.Size(121, 21);
			this.tipEksEdit.TabIndex = 3;
			this.tipEksEdit.SelectedIndexChanged += new System.EventHandler(this.tipEksEdit_SelectedIndexChanged);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(163, 106);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(75, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "Tip eksponata";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(27, 196);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(211, 23);
			this.button1.TabIndex = 7;
			this.button1.Text = "OK";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(163, 149);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(62, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Id umetnika";
			// 
			// idUmEdit
			// 
			this.idUmEdit.Location = new System.Drawing.Point(27, 146);
			this.idUmEdit.Mask = "00000";
			this.idUmEdit.Name = "idUmEdit";
			this.idUmEdit.Size = new System.Drawing.Size(121, 20);
			this.idUmEdit.TabIndex = 5;
			this.idUmEdit.TextChanged += new System.EventHandler(this.idUmEdit_TextChanged);
			// 
			// EksponatiEdit
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(265, 246);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.idUmEdit);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.tipEksEdit);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.tipEdit);
			this.Controls.Add(this.pozEdit);
			this.Name = "EksponatiEdit";
			this.Text = "EksponatiEdit";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EksponatiEdit_FormClosed);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.CheckBox pozEdit;
		private System.Windows.Forms.ComboBox tipEdit;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox tipEksEdit;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.MaskedTextBox idUmEdit;
	}
}