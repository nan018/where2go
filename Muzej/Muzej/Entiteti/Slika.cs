﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Muzej.Entiteti
{
	public class Slika : Eksponat
	{
		public virtual String Tehnika { get; set; }
		public virtual String Stil { get; set; }
		public virtual String OriginalRam { get; set; }
		public virtual String Materijal { get; set; }
		public virtual Decimal Duzina { get; set; }
		public virtual Decimal Sirina { get; set; }
	}
}
