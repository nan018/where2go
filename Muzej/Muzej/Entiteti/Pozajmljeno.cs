﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Muzej.Entiteti
{
    public class Pozajmljeno : Eksponat
    {
        public virtual String NazivKolekcije { get; set; }
        public virtual String Adresa { get; set; }
        public virtual String Opis { get; set; }
        public virtual String IsPrivateOwner { get; set; }

        public Pozajmljeno()
        {

        }

    }
}
