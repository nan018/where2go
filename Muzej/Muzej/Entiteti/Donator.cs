﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace Muzej.Entiteti
{
    [Serializable]
    [DataContract]
    public abstract class Donator
    {
        [DataMember]
        public virtual int Id { get; set; }
        [DataMember]
        public virtual String Adresa { get; set; }
        [DataMember]
        public virtual String Telefon { get; set; }
        [DataMember]
        public virtual String Ime { get; set; }
        [DataMember]
        public virtual String IsFirma { get; set; }
        [DataMember]
        public virtual String OblastPoslovanja { get; set; }
        [DataMember]
        public virtual IList<Donira> DoniraEksponat { get; set; }

        public Donator() 
        {
            DoniraEksponat = new List<Donira>();
        }

    }
    public class Firma : Donator
    {
        //public virtual String OblastPoslovanja { get; set; }

        public Firma()
        {
            IsFirma = "Da";
        }
    }
    public class Privatno : Donator
    {
        public Privatno()
        {
            IsFirma = "Ne";
        }
    }
}
