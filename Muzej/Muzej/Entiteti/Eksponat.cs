﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Muzej.Entiteti
{
    public class Eksponat
    {
        public virtual int Id { get; set; }
        public virtual String IsPozajmljen { get; set; }
        public virtual String Tip { get; set; }
        public virtual String TipEksponata { get; set; }
        public virtual Umetnik Umetnik { get; set; }

        public virtual IList<Donira> DonatorDonirao { get; set; }
		public virtual IList<Izlozen_Na> IzlozenNaIzlozbe { get; set; }

		public Eksponat()
        {
            DonatorDonirao = new List<Donira>();
			IzlozenNaIzlozbe = new List<Izlozen_Na>();
		}
    }

  
}
