﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
    using System.Runtime.Serialization;
namespace Muzej.Entiteti
{


    [Serializable]
    [DataContract]
    public class Donira
    {
        [DataMember]
        public virtual int Id { get; set; }
          [DataMember]
        public virtual decimal Iznos { get; set; }
          [DataMember]
        public virtual DateTime DatumDonacije { get; set; }
          [DataMember]

        public virtual Donator Donator { get; set; }
          [DataMember]
        public virtual Eksponat Eksponat { get; set; }

        public Donira()
        {
           
        }
    }
}
