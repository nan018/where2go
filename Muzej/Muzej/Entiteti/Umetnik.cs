﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
namespace Muzej.Entiteti
{
    [Serializable]
    [DataContract]
    public class Umetnik
    {
        [DataMember]
        public virtual int Id { get; set; }
        [DataMember]
        public virtual String Ime { get; set; }
        [DataMember]
        public virtual String GlavniStil { get; set; }
        [DataMember]
        public virtual String GlavnaTehnika { get; set; }
        [DataMember]
        public virtual String ZemljaPorekla { get; set; }
        [DataMember]
        public virtual String Epoha { get; set; }
        [DataMember]
        public virtual DateTime DatumRodjenja { get; set; }
        [DataMember]
        public virtual DateTime? DatumSmrti { get; set; }
        [DataMember]

        public virtual IList<Eksponat> Eksponati { get; set; }

        public Umetnik()
        {
            Eksponati = new List<Eksponat>();
        }
    }
}
