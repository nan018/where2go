﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Muzej.Entiteti
{
	public class Statua : Eksponat
	{
		public virtual String Stil { get; set; }
		public virtual Decimal Tezina { get; set; }
		public virtual String Materijal { get; set; }
		public virtual Decimal Visina { get; set; }
	}
}
