﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Muzej.Entiteti
{
	public class Izlozen_Na
	{
		public virtual int Id { get; set; }

		public virtual Eksponat IzlazeSeEksponat { get; set; }
		public virtual Izlozba OdrzavaSeIzlozba { get; set; }

        public Izlozen_Na()
        {


        }
	}
}
