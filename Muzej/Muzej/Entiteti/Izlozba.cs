﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Muzej.Entiteti
{
	public class Izlozba
	{
		public virtual int Id { get; set; }
		public virtual String Ime { get; set; }
		public virtual DateTime DatumPocetka { get; set; }
		public virtual DateTime DatumZavrsetka { get; set; }

		public virtual IList<Izlozen_Na> IzlozbaEksponata { get; set; }

		public Izlozba()
		{
            IzlozbaEksponata = new List<Izlozen_Na>();
		}
	}
}
