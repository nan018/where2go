﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Muzej
{
   
    public class EksponatPregled
    {
        public int EksponatId { get; set; }
        public string Pozajmljen { get; set; }
        public string Tip { get; set; }
        public string TipEksponata { get; set; }
        public int? UmetnikId { get; set; }


        public EksponatPregled(int ekId, string poz, string tip, string tipE,int idU)
        {
            this.EksponatId = ekId;
            this.Pozajmljen = poz;
            this.Tip = tip;
            this.TipEksponata = tipE;
            this.UmetnikId = idU;
        }
        public EksponatPregled() { }
    }

}
