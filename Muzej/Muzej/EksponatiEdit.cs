﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Muzej
{
	public partial class EksponatiEdit : Form
	{
		public EksponatPregled ePreg;
		public EksponatiInformacije ei;

		public EksponatiEdit()
		{
			InitializeComponent();
		}

		public EksponatiEdit(EksponatPregled ee, EksponatiInformacije ei)
		{
			this.ePreg = ee;
			this.ei = ei;
			InitializeComponent();
			PopulateData();
		}

		private void PopulateData()
		{
			if (ePreg.Pozajmljen == "Da")
				pozEdit.Checked = true;

			if (ePreg.Tip == "slika")
				tipEdit.SelectedIndex = 0;
			else if (ePreg.Tip == "statua")
				tipEdit.SelectedIndex = 1;
			else if (ePreg.Tip == "pozajmljeno")
				tipEdit.SelectedIndex = 2;
			else if (ePreg.Tip == "ostalo")
				tipEdit.SelectedIndex = 3;

			if (ePreg.TipEksponata == "fotografija")
				tipEksEdit.SelectedIndex = 0;
			else if (ePreg.TipEksponata == "stampani materijal")
				tipEksEdit.SelectedIndex = 1;
			else if (ePreg.TipEksponata == "predmet")
				tipEksEdit.SelectedIndex = 2;

			idUmEdit.Text = ePreg.UmetnikId.ToString();
		}

		private void pozEdit_Click(object sender, EventArgs e)
		{
			if(pozEdit.Checked == true)
			{
				ePreg.Pozajmljen = "Da";
			}
			else
			{
				ePreg.Pozajmljen = "Ne";
			}
		}

		private void tipEdit_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (tipEdit.SelectedIndex == 0)
				ePreg.Tip = "slika";
			else if (tipEdit.SelectedIndex == 1)
				ePreg.Tip = "statua";
			else if (tipEdit.SelectedIndex == 2)
				ePreg.Tip = "pozajmljeno";
			else if (tipEdit.SelectedIndex == 3)
				ePreg.Tip = "ostalo";
		}

		private void tipEksEdit_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (tipEksEdit.SelectedIndex == 0)
				ePreg.TipEksponata = "fotografija";
			else if (tipEksEdit.SelectedIndex == 1)
				ePreg.TipEksponata = "stampani materijal";
			else if (tipEksEdit.SelectedIndex == 2)
				ePreg.TipEksponata = "predmet";
		}

		private void idUmEdit_TextChanged(object sender, EventArgs e)
		{
			ePreg.UmetnikId = Convert.ToInt32(idUmEdit.Text);
		}

		private void button1_Click(object sender, EventArgs e)
		{
			DTOManager.UpdateEksponatPregled(ePreg);
			this.Close();
		}

		private void EksponatiEdit_FormClosed(object sender, FormClosedEventArgs e)
		{
			ei.PopulateInfos();
		}
	}
}
