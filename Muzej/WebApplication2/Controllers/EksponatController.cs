﻿using Muzej;
using Muzej.DTOs;
using Muzej.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;


namespace WebApplication2.Controllers
{

    public class EksponatController : ApiController
    {
        public List<Eksponat> Get()
        {
            DataProvider provider = new DataProvider();

            IEnumerable<Eksponat> eksponati = provider.GetEksponati();

            List<Eksponat> le = eksponati.ToList<Eksponat>();
            foreach (Eksponat item in le)
            {

                if (item.Umetnik != null)
                {
                    Umetnik u = new Umetnik
                    {
                        Ime = item.Umetnik.Ime,
                        Id = item.Id,
                        DatumSmrti = item.Umetnik.DatumSmrti,
                        DatumRodjenja = item.Umetnik.DatumRodjenja,
                        Epoha = item.Umetnik.Epoha,
                        GlavnaTehnika = item.Umetnik.GlavnaTehnika,
                        GlavniStil = item.Umetnik.GlavniStil,
                        ZemljaPorekla = item.Umetnik.ZemljaPorekla
                    };
                    item.Umetnik=u;
                    
                }
                item.IzlozenNaIzlozbe = null;
                item.DonatorDonirao = null;
            }
            return le;
        }

        public EksponatView Get(int id)
        {
            DataProvider provider = new DataProvider();
        
            Eksponat e = provider.GetEksponat(id);
            if(e.Umetnik!=null){
            Umetnik u = new Umetnik
            {
                Ime = e.Umetnik.Ime,
                Id = e.Id,
                DatumSmrti = e.Umetnik.DatumSmrti,
                DatumRodjenja = e.Umetnik.DatumRodjenja,
                Epoha = e.Umetnik.Epoha,
                GlavnaTehnika = e.Umetnik.GlavnaTehnika,
                GlavniStil = e.Umetnik.GlavniStil,
                ZemljaPorekla = e.Umetnik.ZemljaPorekla
            };
            e.Umetnik = u;
            }
            return new EksponatView(e);
        }

        public bool Post([FromBody]Object eks)
        {
            DataProvider provider = new DataProvider();
            return provider.AddEksponat(eks);
        }

        public bool Put(int id, [FromBody]Object eksponat)
        {
            
            JObject jEks = eksponat as JObject;
            JProperty ss = jEks.Property("tip");

            Eksponat eks = new Eksponat();

            eks.TipEksponata = jEks.Property("tipEksponata").Value.ToString();
            eks.Tip = ss.Value.ToString();

            int idUmet = -1;
            if (jEks.Property("idUmetnika")!=null)
            {
                idUmet = Int32.Parse(jEks.Property("idUmetnika").Value.ToString());
            }

            switch (eks.Tip)
            {
                case "pozajmljeno":
                    return Muzej.Funkcije.EksponatFunkcije.izmeni_pozajmljeno(id, eks.Tip, jEks.Property("nazivKolekcije").Value.ToString(), jEks.Property("adresa").Value.ToString(),
                        jEks.Property("opis").Value.ToString(), (jEks.Property("isPrivateOwner").Value.ToString() == "Da") ? true : false, eks.TipEksponata, true, idUmet);

                case "ostalo":
                    DataProvider p = new DataProvider();
                    return p.UpdateEksponat(id, eks.Tip, eks.TipEksponata, idUmet); 
                default:
                    break;
            }

            return false;
        }

        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.RemoveEksponat(id);
        }
	}
}