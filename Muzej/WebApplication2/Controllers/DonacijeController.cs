﻿using Muzej;
using Muzej.DTOs;
using Muzej.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;

namespace WebApplication2.Controllers
{
    public class DonacijeController : ApiController
    {

        public Donira Get(int id)
        {
            DataProvider provider = new DataProvider();
            Donira don;
            don = provider.GetDonaciju(id);
            don.Donator.DoniraEksponat = null;
            don.Eksponat.DonatorDonirao = null;
            don.Eksponat.IzlozenNaIzlozbe = null;
            Donator donator;
            Firma firma = new Firma();
            if (don.Donator.IsFirma == "Da")
            {
                donator = new Firma();
            }
            else
            {
                donator = new Privatno();
            }
            donator.Ime = don.Donator.Ime;
            donator.Id = don.Donator.Id;
            donator.Adresa = don.Donator.Adresa;
            donator.IsFirma = don.Donator.IsFirma;
            donator.OblastPoslovanja = don.Donator.OblastPoslovanja;
            donator.Telefon = don.Donator.Telefon;
            don.Donator = donator;
            Eksponat eks = new Eksponat();
            eks.Id = don.Eksponat.Id;
            eks.IsPozajmljen = don.Eksponat.IsPozajmljen;
            eks.Tip = don.Eksponat.Tip;
            eks.TipEksponata = don.Eksponat.TipEksponata;
            don.Eksponat = eks;
            return don;
        }

        public List<Donira> Get()
        {
            DataProvider provider = new DataProvider();
            List<Donira> donacije = provider.GetDonacije().ToList<Donira>();
            donacije.ForEach((don) => { don.Eksponat = null; don.Donator = null; });
            return donacije;
        }

        public bool Post([FromBody]Object donacija)
        {
            JObject jo = donacija as JObject;
            return Muzej.Funkcije.DonatorFunkcije.doniraj(int.Parse(jo.Property("idDonatora").Value.ToString()), int.Parse(jo.Property("idEksponata").Value.ToString()), 
                decimal.Parse(jo.Property("Iznos").Value.ToString()), DateTime.Parse(jo.Property("DatumDonacije").Value.ToString()));
        }
       // Nema potrebe za izmenu donacije
        public bool Delete(int id)
        {
            return Muzej.Funkcije.DonatorFunkcije.obrisi_donaciju(id);
        }



    }
}