﻿using Muzej;
using Muzej.DTOs;
using Muzej.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;

namespace WebApplication2.Controllers
{
    public class DonatorController : ApiController
    {
        public List<Donator> Get()
        {
            DataProvider provider = new DataProvider();
            List<Donator> donatori = provider.GetDonatori().ToList<Donator>();
            donatori.ForEach((don) => { don.DoniraEksponat = null; });
            return donatori;
        }
        public Donator Get(int id)
        {
            DataProvider provider = new DataProvider();
            Donator don = provider.GetDonator(id);
            don.DoniraEksponat = null;
            return don;
        }
        public bool Post([FromBody]Object donator)
        {
            JObject jEks = donator as JObject;

            Donator don;
            JProperty isFirma = jEks.Property("IsFirma");
            if (isFirma.Value.ToString() == "Da")
                don = new Firma();
            else
                don = new Privatno();
            don.Adresa = jEks.Property("Adresa").Value.ToString();
            don.Telefon = jEks.Property("Telefon").Value.ToString();
            don.Ime = jEks.Property("Ime").Value.ToString();
            don.OblastPoslovanja = jEks.Property("OblastPoslovanja").Value.ToString();
            don.IsFirma = jEks.Property("IsFirma").Value.ToString();
            return Muzej.Funkcije.DonatorFunkcije.kreiraj_donatora(don.Adresa, don.Telefon, don.Ime, don.OblastPoslovanja, (don.IsFirma=="Da")?true:false);
        }
        public bool Put(int id, [FromBody]Object donator)
        {
            JObject jEks = donator as JObject;

            Donator don;
            JProperty isFirma = jEks.Property("IsFirma");
            if (isFirma.Value.ToString() == "Da")
                don = new Firma();
            else
                don = new Privatno();
            don.Adresa = jEks.Property("Adresa").Value.ToString();
            don.Telefon = jEks.Property("Telefon").Value.ToString();
            don.Ime = jEks.Property("Ime").Value.ToString();
            don.OblastPoslovanja = jEks.Property("OblastPoslovanja").Value.ToString();
            don.IsFirma = jEks.Property("IsFirma").Value.ToString();
            return Muzej.Funkcije.DonatorFunkcije.izmeni_donatora(id, don.Adresa, don.Telefon, don.Ime, don.IsFirma);
        }
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.RemoveDonator(id);
        }
    }
}