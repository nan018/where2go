﻿using Muzej;
using Muzej.DTOs;
using Muzej.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;

namespace WebApplication2.Controllers
{
    public class UmetnikController : ApiController
    {

        public List<Umetnik> Get()
        {
            DataProvider provider = new DataProvider();
           List<Umetnik> umetnici = provider.GetUmetnici().ToList<Umetnik>();
           umetnici.ForEach((e) => { e.Eksponati = null; });
                return umetnici;
        }


        public Umetnik Get(int id)
        {
            DataProvider provider = new DataProvider();
            Umetnik e = provider.GetUmetnik(id);
            e.Eksponati = null;
            return e;
        }

        public bool Post([FromBody]Umetnik umetnik)
        {
            return Muzej.Funkcije.UmetnikFunkcije.kreiraj_umetnika(umetnik.DatumRodjenja, umetnik.DatumSmrti, umetnik.Ime, umetnik.ZemljaPorekla, umetnik.Epoha, umetnik.GlavniStil, umetnik.GlavnaTehnika);
        }
        public bool Put(int id,[FromBody]Umetnik umetnik)
        {
            return Muzej.Funkcije.UmetnikFunkcije.izmeni_umetnika(id, umetnik.Epoha, umetnik.GlavniStil,umetnik.GlavnaTehnika);
        }
        public int Delete(int id)
        {
            DataProvider provider = new DataProvider();
            return provider.RemoveUmetnik(id);
        }

    }
}